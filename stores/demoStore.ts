import create from 'zustand';

type DemoStoreT = {
  count: number;
  incrementCount: () => void;
};

export const useDemoStore = create<DemoStoreT>((set) => ({
  count: 0,
  incrementCount: () => set((state) => ({ count: state.count + 1 })),
}));
