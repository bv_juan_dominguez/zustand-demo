Esta es una demo de como usar Zustand en un proyecto de Next.js. Incluye un store, componentes que leen los estados y componentes que se informan de manera transitiva (sin generar re-rnder) de los valores del store.

## Habilitar marcas de render

Para visualizar los renders al presionar los botones de la demo debes:

1. Tener instalada la extensión [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=es) en tu navegador.

2. Habilitar la opción **"Highlight updates when components render"** en la consola de React Developer Tools.

![image.png](./image.png)

Nota: Para ver las marcas debe estar la consola abierta
