import type { NextPage } from 'next';
import Counter from '../components/Counter';
import IncrementButton from '../components/IncrementButton';
import NonTransientButton from '../components/NonTransientButton';
import TransientButton from '../components/TransientButton';
import TransientCounter from '../components/TransientCounter';

const Demo: NextPage = () => {
  return (
    <div>
      <Counter />

      <IncrementButton />

      <NonTransientButton />

      <TransientButton />

      <TransientCounter />
    </div>
  );
};

export default Demo;
