import { useEffect, useRef } from 'react';
import { useDemoStore } from '../stores/demoStore';

const TransientButton = () => {
  const countRef = useRef(useDemoStore.getState().count);

  useEffect(() => useDemoStore.subscribe((state) => (countRef.current = state.count)), []);

  return <button onClick={() => alert('Count is: ' + countRef.current)}>Transient</button>;
};

export default TransientButton;
