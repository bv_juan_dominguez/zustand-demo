import { useDemoStore } from '../stores/demoStore';

const Counter = () => {
  const count = useDemoStore((s) => s.count);

  return <h1>{count}</h1>;
};

export default Counter;
