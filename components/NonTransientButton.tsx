import { useDemoStore } from '../stores/demoStore';

const NonTransientButton = () => {
  const count = useDemoStore((s) => s.count);

  return <button onClick={() => alert('Count is: ' + count)}>Non Transient</button>;
};

export default NonTransientButton;
