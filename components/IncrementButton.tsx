import { useDemoStore } from '../stores/demoStore';

const IncrementButton = () => {
  const incrementCount = useDemoStore((s) => s.incrementCount);

  return <button onClick={() => incrementCount()}>Increment</button>;
};

export default IncrementButton;
