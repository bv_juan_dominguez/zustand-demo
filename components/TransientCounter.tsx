import { useEffect, useRef } from 'react';
import { useDemoStore } from '../stores/demoStore';

const TransientCounter = () => {
  const countRef = useRef(useDemoStore.getState().count);

  useEffect(() => useDemoStore.subscribe((state) => (countRef.current = state.count)), []);

  return <h1>{countRef.current}</h1>;
};

export default TransientCounter;
